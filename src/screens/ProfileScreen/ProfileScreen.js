import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput, ImageBackground, Modal } from 'react-native';
import { fetchAxios } from '../../utils/fetchAxios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import avatar from '../../assets/avatar.jpeg';
import imgBackground from '../../assets/background.jpeg'
import styles, { buttonStyles } from './ProfileScreen-style';


function ProfileScreen(props) {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [age, setAge] = useState('')
    const [totalTask, setTotalTask] = useState('')
    const [completedTask, setCompletedTask] = useState('')
    const [modalEditProfile, setModalEditProfile] = useState(false)
    const [modalLogOut, setModalLogOut] = useState(false)
    const [modalDelProfile, setModalDelProfile] = useState(false)

    useEffect(() => {
        const { navigation } = props;
        const refresh = navigation.addListener('focus', () => {
            getUser();
            getTask();
            getCompletedTask();
        });
    }, [])

    const getUser = async () => {
        try {
            const token = await AsyncStorage.getItem('@token')
            const response = await fetchAxios("GET", 'user/me', '', {
                'Authorization': `${token}`
            });
            console.log('res: == ', response.data);
            setName(response.data.name);
            setEmail(response.data.email);
            setAge(response.data.age);
        } catch (e) {
            console.error(e);
        }
    }

    const getTask = async () => {
        try {
            const token = await AsyncStorage.getItem('@token')
            const response = await fetchAxios("GET", 'task', '', {
                'Authorization': `${token}`,
                'Content-Type': 'application/json'
            })
            console.log('res: == ', response.data);
            setTotalTask(response.data.count);
        } catch (e) {
            console.error(e);
        }
    }

    const getCompletedTask = async () => {
        const token = await AsyncStorage.getItem('@token')
        try {
            const response = await fetchAxios("GET", 'task?completed=true', '', {
                'Authorization': `${token}`,
                'Content-Type': 'application/json'
            })
            console.log('res: == ', response.data);
            setCompletedTask(response.data.count);

        } catch (error) {
            console.error(error);
        }
    }

    const signOut = async () => {
        const { navigation } = props;
        const token = await AsyncStorage.getItem('@token')
        const response = await fetchAxios("POST", 'user/logout', '', {
            'Authorization': `${token}`,
            'Content-Type': 'application/json'
        });
        console.log('res: == ', response.data);
        console.log(response.data.success);
        if (response.data.success == true) {
            await AsyncStorage.removeItem('@token');
            navigation.navigate('LandingPage');
        } else {
            alert('error');
        }
    }

    const deleteUser = async () => {
        const { navigation } = props;
        const token = await AsyncStorage.getItem('@token')
        const response = await fetchAxios("DELETE", 'user/me', '', {
            'Authorization': `${token}`,
        })
        console.log('res: == ', response.data);
        await AsyncStorage.removeItem('@token');
        navigation.navigate('LandingPage');
    }

    const editUser = async () => {
        const { navigation } = props;
        const token = await AsyncStorage.getItem('@token')
        const response = await fetchAxios("PUT", 'user/me', {
            "name": name,
            "email": email,
            "age": age
        }, {
            'Authorization': `${token}`,
            'Content-Type': 'application/json'
        })
        console.log('res: == ', response.data);
        getUser()
        setModalEditProfile(false)

    }


    return (
        <ImageBackground source={imgBackground} style={styles.image}>
            <View>
                <View style={styles.centerimage}>
                    <Image
                        style={styles.image2}
                        resizeMode={"cover"}
                        source={avatar}
                    />
                    <Text style={styles.text} >{name} ({age} thn)</Text>
                    <Text style={styles.text} >{email}</Text>
                </View>

                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'space-evenly', marginTop: 20 }}>
                    <View>
                        <Text style={styles.text1}>{totalTask}</Text>
                        <Text style={styles.text2}>Total Task</Text>
                    </View>
                    <View>
                        <Text style={styles.text1}>{completedTask}</Text>
                        <Text style={styles.text2}>Task Complete</Text>
                    </View>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => {
                            setModalDelProfile(true)
                        }}>
                        <Text style={styles.text3} >Delete User</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => {
                            setModalEditProfile(true);
                        }}>
                        <Text style={styles.text3} >Edit User</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => {
                            setModalLogOut(true)
                        }}>
                        <Text style={styles.text3} >Sign Out</Text>
                    </TouchableOpacity>
                </View>

                <Modal transparent={true} visible={modalDelProfile}>
                    <View style={{ backgroundColor: '#000000aa', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ backgroundColor: 'white', padding: '5%', width: '70%', borderRadius: 40 }}>
                            <Text style={styles.text2}>Are you sure to delete ?</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20 }}>
                                <TouchableOpacity
                                    style={styles.buttonYes}
                                    onPress={() => {
                                        deleteUser();
                                    }}>
                                    <Text style={styles.text3} >Delete</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.buttonNo}
                                    onPress={() => {
                                        setModalDelProfile(false)
                                    }}>
                                    <Text style={styles.text3} >Cancel</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>

                <Modal transparent={true} visible={modalLogOut}>
                    <View style={{ backgroundColor: '#000000aa', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ backgroundColor: 'white', padding: '5%', width: '70%', borderRadius: 40 }}>
                            <Text style={styles.text2}>Are you sure to Sign Out ?</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20 }}>
                                <TouchableOpacity
                                    style={styles.buttonYes}
                                    onPress={() => {
                                        signOut();
                                    }}>
                                    <Text style={styles.text3} >Sign Out</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.buttonNo}
                                    onPress={() => {
                                        setModalLogOut(false)
                                    }}>
                                    <Text style={styles.text3} >Cancel</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>

                <Modal transparent={true} visible={modalEditProfile}>
                    <View style={{ backgroundColor: '#000000aa', flex: 1, justifyContent: 'space-evenly', alignItems: 'center' }}>
                        <View style={{ backgroundColor: 'white', padding: '5%', width: '70%', borderRadius: 40 }}>
                            <Text style={styles.text2}>Edit Your Profile</Text>
                            <View style={{ marginTop: 10 }}>
                                <Text style={styles.text4}>Name :</Text>
                                <TextInput
                                    style={styles.TextInput}
                                    placeholder="Place your name"
                                    placeholderTextColor="grey"
                                    onChangeText={(val) => setName(val)}
                                >
                                </TextInput>
                            </View>
                            <View>
                                <Text style={styles.text4}>Email :</Text>
                                <TextInput
                                    style={styles.TextInput}
                                    placeholder="Place your email"
                                    placeholderTextColor="grey"
                                    onChangeText={(val) => setEmail(val)}
                                >
                                </TextInput>
                            </View>
                            <View>
                                <Text style={styles.text4}>Age :</Text>
                                <TextInput
                                    style={styles.TextInput}
                                    placeholder="Place your age"
                                    placeholderTextColor="grey"
                                    onChangeText={(val) => setAge(val)}
                                >
                                </TextInput>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20 }}>
                                <TouchableOpacity
                                    style={styles.buttonYes}
                                    onPress={() => {
                                        editUser()
                                    }}>
                                    <Text style={styles.text3} >Save</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.buttonNo}
                                    onPress={() => {
                                        setModalEditProfile(false)
                                    }}>
                                    <Text style={styles.text3} >Cancel</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        </ImageBackground>
    )
}

export default ProfileScreen;



