import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
    },

    image2: {
        height: 80,
        width: 80,
        borderRadius: 50,
        justifyContent: 'center',
        marginBottom: 10,
    },

    centerimage : {
        alignItems: 'center',
    },

    text: {
        fontSize: 24,
        color: "black",
        textAlign: 'center',
        marginBottom:10,
        fontWeight: 'bold'
    },

    text1: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 20,
        color: "black",
        textAlign: 'center',
        fontWeight: 'bold'
    },

    text2: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 18,
        color: "black",
        textAlign: 'center',
    },

    text3: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 15,
        color: "black",
        textAlign: 'center',
    },

    text4: {
        marginLeft: 10,
        fontSize: 15,
        color: "black",
        textAlign: 'left',
    },


    buttonYes: {
		width: 80,
		backgroundColor: "lightgreen",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginBottom: 10,
	},
	buttonNo: {
		width: 80,
		backgroundColor: "#FFCCCB",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginBottom: 10,
	},
	

	button: {
		width: 300,
		backgroundColor: "lightblue",
		borderRadius: 50,
		height: 40,
		alignItems: "center",
		justifyContent: "center",
		marginTop: 10,
		marginBottom: 5,

	},
	TextInput: {
        borderWidth: 1,
        padding : 10,
        borderColor: 'black',
        height: 40,
        borderRadius: 50,
        marginTop: 10,
        marginBottom: 10,
        color: 'black',
    },
});

export default styles;