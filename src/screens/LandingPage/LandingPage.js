import React from 'react';
import { View, Text, TouchableOpacity, ImageBackground, Image} from 'react-native';
import imgBackground from '../../assets/background.jpeg'
import logo from '../../assets/logo.jpeg'
import styles, { buttonStyles } from './LandingPage-style';

class LandingPage extends React.Component {
  render() {
    return (
    <ImageBackground source={imgBackground} style={styles.image}>
      <View>
        <View style={styles.centerimage}>
            <Image 
                style={styles.image2}
                resizeMode={"cover"}
                source={logo}
            />
        </View>
        <Text style={styles.text}>Stark Industries</Text>
        <Text style={styles.text1}>Memproduksi baterai dan barang-barang elektronik lainnya. Harga bersaing!!!</Text>
        <View style={styles.centerbutton}> 
            <Button1
                title="MASUK"
                onPress={() => this.props.navigation.navigate('LoginScreen')} 
            />
        </View>
        
        <View style={styles.centerbutton}> 
            <Button1
                title="DAFTAR"
                onPress={() => this.props.navigation.navigate('RegisterScreen')}
            />
        </View>
      </View>
      </ImageBackground>
    );
  }
}

const Button1 = ({ title, onPress }) => {
  return (
    <TouchableOpacity style={buttonStyles.container} onPress={onPress}>
      <Text>{title.toUpperCase()}</Text>
    </TouchableOpacity>
  )
}

export default LandingPage