import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
    },

    image2: {
        height: 80,
        width: 80,
        borderRadius: 50,
        justifyContent: 'center',
        marginBottom: 50,
    },

    centerimage : {
        alignItems: 'center',
    },

    centerbutton : {
        flexDirection:'row', 
        justifyContent: 'center',
    },

    text: {
        fontSize: 24,
        color: "black",
        textAlign: 'center',
        marginBottom:10,
        fontWeight: 'bold'
    },
    text1: {
        fontSize: 18,
        color: "black",
        textAlign: 'center',
        marginBottom: 50,
        marginLeft:20,
        marginRight:20,
    }
});

export const buttonStyles = StyleSheet.create({
    container: {
        backgroundColor: 'lightblue',
        height: 40,
        width: '80%' ,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        padding: '3%',
        marginBottom: 10,
        marginTop: 10
    },
    Text: {
        fontSize: 24,
        color: 'black',
    },
});

export default styles;