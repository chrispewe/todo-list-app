import React from 'react';
import { View, Text, Button, TouchableOpacity, ImageBackground, Image, TextInput } from 'react-native';
import imgBackground from '../../assets/background.jpeg'
import logo from '../../assets/logo.jpeg'
import { fetchRegister } from '../../utils/fetchAxios'

import styles, { buttonStyles } from './RegisterScreen-style';

class RegisterScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: 'budi3',
            email: 'budi3@email.com',
            password: '12345678',
            age: '29',
        }
    }

    handleRegister(val) {
        fetchRegister(val)
    }

    render() {
        const { name, email, password, age } = this.state
        return (
            <ImageBackground source={imgBackground} style={styles.image}>

                <View>
                    <View style={styles.centerimage}>
                        <Image
                            style={styles.image2}
                            resizeMode={"cover"}
                            source={logo} />
                    </View>

                    <Text style={styles.text1}>Daftar ke Stark Industries</Text>
                    <View style={styles.centerinput}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Name"
                            placeholderTextColor="#F4B400"
                            onChangeText={(val) => this.setState({ name: val })}
                        />
                    </View>
                    <View style={styles.centerinput}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Email"
                            placeholderTextColor="#F4B400"
                            onChangeText={(val) => this.setState({ email: val })}
                        />
                    </View>
                    <View style={styles.centerinput}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Password"
                            placeholderTextColor="#F4B400"
                            onChangeText={(val) => this.setState({ password: val })}
                            secureTextEntry={true}
                        />
                    </View>
                    <View style={styles.centerinput}>
                        <TextInput
                            style={styles.textInput}
                            placeholder="Age"
                            placeholderTextColor="#F4B400"
                            onChangeText={(val) => this.setState({ age: val })}
                        />
                    </View>
                    <View style={styles.centerbutton}>
                        <Button1
                            title="DAFTAR"
                            onPress={() => this.handleRegister({ name, email, password, age })}
                        />
                    </View>
                    <Text style={styles.text3}>Sudah Punya Akun?</Text>
                    <Text
                        style={styles.text4}
                        onPress={() => this.props.navigation.navigate('LoginScreen')}>
                        Masuk Disini
            </Text>
                </View>

            </ImageBackground>
        );
    }
}

const Button1 = ({ title, onPress }) => {
    return (
        <TouchableOpacity style={buttonStyles.container} onPress={onPress}>
            <Text>{title.toUpperCase()}</Text>
        </TouchableOpacity>
    )
}

export default RegisterScreen