import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
   
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
    },

    image2: {
        height: 80,
        width: 80,
        borderRadius: 50,
        justifyContent: 'center',
        marginBottom: 50,
    },

    centerimage : {
        alignItems: 'center',
    },

    centerbutton : {
        flexDirection:'row', 
        justifyContent: 'center',
    },

    text: {
        fontSize: 24,
        color: "black",
        textAlign: 'center',
        marginBottom:10,
        fontWeight: 'bold'
    },
    text1: {
        fontSize: 18,
        color: "black",
        textAlign: 'center',
        marginBottom: 10,
        marginTop:10,
        marginLeft:20,
        marginRight:20,
        fontWeight: 'bold'
    },
    text3: {
        fontSize: 15,
        color: "black",
        textAlign: "center",
        marginLeft:20,
        marginRight:20,
        marginTop:10,
    },
    text4: {
        fontSize: 15,
        color: "black",
        textAlign: "center",
        marginLeft:20,
        marginRight:20,
        fontWeight: 'bold'
    },
    textInput: {
        borderWidth: 1,
        padding : 10,
        borderColor: 'black',
        height: 40,
        width: '80%' ,
        borderRadius: 50,
        marginTop: 10,
        marginBottom: 10,
        color: 'black',
    },

    centerinput : {
        flexDirection:'row', 
        justifyContent: 'center',
    },
});

export const buttonStyles = StyleSheet.create({
    container: {
        backgroundColor: 'lightblue',
        height: 40,
        width: '80%' ,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        padding: '3%',
        marginBottom: 10,
        marginTop: 10
    },
    Text: {
        fontSize: 24,
        color: 'black',
    },
});

export default styles;