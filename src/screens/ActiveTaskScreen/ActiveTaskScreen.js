import React, { useState, useEffect, } from 'react';
import { Card, Divider } from 'react-native-elements';
import { View, Text, TouchableOpacity, ImageBackground, Image, TextInput, FlatList, TouchableHighlight, Modal } from 'react-native';
import { fetchAxios } from '../../utils/fetchAxios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import styles from './ActiveTaskScreen-style';
import imgBackground from '../../assets/background.jpeg'


function ActiveTaskScreen(props) {
    const [completedTask, setCompletedTask] = useState([]);
    const [desc, setDesc] = useState('')
    const [selectedData, setSelectedData] = useState({})
    const [modalAdd, setModalAdd] = useState(false);
    const [modalUpdate, setModalUpdate] = useState(false)
    const [modalDelete, setModalDelete] = useState(false)

    useEffect(() => {
        const { navigation } = props;
        const refresh = navigation.addListener('focus', () => {
            getTask();
        });
    }, [])

    const getTask = async () => {
        try {
            const token = await AsyncStorage.getItem('@token')
            const response = await fetchAxios("GET", 'task?completed=false', '', {
                'Authorization': `${token}`,
                'Content-Type': 'application/json'
            })
            console.log('res: == ', response.data);
            setCompletedTask(response.data.data)
        } catch (e) {
            console.error(e);
        }
    }

    const addTask = async () => {
        try {
            const token = await AsyncStorage.getItem('@token')
            const response = await fetchAxios("POST", 'task', {
                "description": `${desc}`
            }, {
                'Authorization': `${token}`,
                'Content-Type': 'application/json'
            })
            console.log(response.data.success)
            if (response.data.success == true) {
                getTask();
                setModalAdd(false)

            }
        } catch (e) {
            console.error(e);
        }
    }

    const updateTask = async (id, completed) => {
        try {
            if (completed == true) {
                update = false
            } else {
                update = true
            }
            const token = await AsyncStorage.getItem('@token')
            const response = await fetchAxios("PUT", `task/${id}`, {
                "completed": update
            }, {
                'Authorization': `${token}`,
                'Content-Type': 'application/json'
            })
            console.log(response.data.success)
            if (response.data.success == true) {
                getTask();
                setModalUpdate(false)
                setSelectedData({});
            }

        } catch (e) {
            console.error(e);
        }
    }
    const deleteTask = async (id) => {
        try {
            const token = await AsyncStorage.getItem('@token')
            const response = await fetchAxios("DELETE", `task/${id}`, '', {
                'Authorization': `${token}`,
                'Content-Type': 'application/json'
            })
            console.log(response.data.success)
            if (response.data.success == true) {
                getTask();
                setModalDelete(false)
                setSelectedData({});
            }

        } catch (e) {
            console.error(e);
        }
    }
    renderItem = ({ item }) => {
        return (
            <View>
                <Card containerStyle={styles.cardTask}>
                    <Card.Title style={styles.text4}>{item.description}</Card.Title>
                    {item.completed ? (
                        <Text style={styles.text4}>Status : Completed</Text>

                    ) : (
                            <Text style={styles.text4}>Status : Uncompleted</Text>
                        )}
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                        <TouchableOpacity
                            style={styles.buttonDelete}
                            onPress={() => {
                                setSelectedData(item);
                                setModalDelete(true)
                            }}>
                            <Text style={styles.text4}>Delete Task</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.buttonUpdate}
                            onPress={() => {
                                setSelectedData(item);
                                setModalUpdate(true)
                            }}>
                            <Text style={styles.text4}>Update Status</Text>
                        </TouchableOpacity>

                    </View>
                </Card>
            </View>
        );
    }

    return (
        <ImageBackground source={imgBackground} style={styles.image}>
        <View style={{ flex: 1 }}>
            <FlatList
                data={completedTask}
                renderItem={renderItem}
                keyExtractor={item => item._id}
            />

            <TouchableOpacity
                style={styles.button}
                onPress={() =>
                    setModalAdd(true)}>
                <Text style={{ color: 'white', fontSize: 35, textAlign: 'center' }}>+</Text>
            </TouchableOpacity>


            <Modal transparent={true} visible={modalAdd}>
                <View style={{ backgroundColor: '#000000aa', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ backgroundColor: 'white', padding: '5%', width: '70%', borderRadius: 40 }}>
                    <Text style={styles.text2}>Add Task</Text>
                        <TextInput
                            style={styles.TextInput}
                            placeholder="Place new task"
                            placeholderTextColor="#003f5c"
                            onChangeText={(value) => setDesc(value)}
                        // value={desc}
                        >
                        </TextInput>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20 }}>
                            <TouchableOpacity
                                style={styles.buttonSave}
                                onPress={() => {
                                    addTask()
                                }}>
                               <Text style={styles.text3} >Save</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.buttonCancel}
                                onPress={() => {
                                    setModalAdd(false)
                                }}>
                                <Text style={styles.text3} >Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>

            <Modal transparent={true} visible={modalUpdate}>
            <View style={{ backgroundColor: '#000000aa', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ backgroundColor: 'white', padding: '5%', width: '70%', borderRadius: 40 }}>
                        <Text style={styles.text2} >Update task to {selectedData.completed ? 'Uncompleted' : 'Completed'}?</Text>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
                            <TouchableOpacity
                                style={styles.buttonYes}
                                onPress={() => {
                                    updateTask(selectedData._id, selectedData.completed)
                                }}>
                                <Text style={styles.text3} >Yes</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.buttonNo}
                                onPress={() => {

                                    setModalUpdate(false)
                                }}>
                                <Text style={styles.text3} >No</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>

            <Modal transparent={true} visible={modalDelete}>
            <View style={{ backgroundColor: '#000000aa', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ backgroundColor: 'white', padding: '5%', width: '70%', borderRadius: 40 }}>
                        <Text style={styles.text2} >Are you sure to Delete Task?</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
                            <TouchableOpacity
                                style={styles.buttonYes}
                                onPress={() => {
                                    deleteTask(selectedData._id)
                                }}>
                                <Text style={styles.text3} >Yes</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.buttonNo}
                                onPress={() => {
                                    setModalDelete(false)
                                }}>
                                 <Text style={styles.text3} >No</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>

        </View>
        </ImageBackground>
    )
}


export default ActiveTaskScreen;


