import React, { useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, Image, TextInput } from 'react-native';
import imgBackground from '../../assets/background.jpeg'
import logo from '../../assets/logo.jpeg'
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import Config from 'react-native-config';
import styles, { buttonStyles } from './LoginScreen-style';

function LoginScreen({ navigation }) {
    const [email, setEmail] = useState('budi3@email.com');
    const [password, setPassword] = useState('12345678');

    const fetchLogin = async (val) => {
        try {
            const response = await axios.post(`${Config.BASE_URL}/user/login`, val);
            console.log('res: == ', response.data)
            if (response.status != 400) {
                AsyncStorage.setItem('@token', response.data.token);
                navigation.navigate('Home');
            } else {
                alert('error');
            }
        } catch (e) {
            console.error(e);
        }
    }

    const handleLogin = async (val) => {
        fetchLogin(val)
    }

    return (
        <ImageBackground source={imgBackground} style={styles.image}>
            <View>
                <View style={styles.centerimage}>
                    <Image
                        style={styles.image2}
                        resizeMode={"cover"}
                        source={logo}
                    />
                </View>

                <Text style={styles.text1}>Halo, Masuk ke Stark Industries</Text>
                <View style={styles.centerinput}>
                    <TextInput
                        style={styles.textInput}
                        placeholder="Email"
                        placeholderTextColor="#F4B400"
                        onChangeText={(val) => setEmail(val)}
                        value={email}
                    />
                </View>
                <View style={styles.centerinput}>
                    <TextInput
                        style={styles.textInput}
                        placeholder="Password"
                        placeholderTextColor="#F4B400"
                        onChangeText={(val) => setPassword(val)}
                        value={password}
                        secureTextEntry={true}
                    />
                </View>
                <View style={styles.centerbutton}>
                    <Button1
                        title="MASUK"
                        onPress={() => handleLogin({ email, password })}
                    />
                </View>
                <Text style={styles.text3}>Belum Punya Akun?</Text>
                <Text
                    style={styles.text4}
                    onPress={() => navigation.navigate('RegisterScreen')}>
                    Daftar Dulu
            </Text>
            </View>

        </ImageBackground>
    );
}


const Button1 = ({ title, onPress }) => {
    return (
        <TouchableOpacity style={buttonStyles.container} onPress={onPress}>
            <Text>{title.toUpperCase()}</Text>
        </TouchableOpacity>
    )
}

export default LoginScreen
