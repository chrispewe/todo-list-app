import { StyleSheet } from 'react-native';

const styles=StyleSheet.create({
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
    },
   
    cardTask: {
        backgroundColor: 'lightblue',
        borderRadius: 16,
        padding: '5%'
    },

    text1: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 20,
        color: "black",
        textAlign: 'center',
        fontWeight: 'bold'
    },

    text2: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 18,
        color: "black",
        textAlign: 'center',
    },

    text3: {
        justifyContent: 'center',
        alignSelf: 'center',
        fontSize: 15,
        color: "black",
        textAlign: 'center',
    },

    text4: {
        fontSize: 15,
        color: "black",
        textAlign: 'center',
    },

	buttonCancel: {
		width: 80,
		backgroundColor: "#FFCCCB",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginBottom: 10,
	},
	buttonSave: {
		width: 80,
		backgroundColor: "lightgreen",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginBottom: 10,
	},
	buttonYes: {
		width: 80,
		backgroundColor: "lightgreen",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginBottom: 10,
	},
	buttonNo: {
		width: 80,
		backgroundColor: "#FFCCCB",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
		justifyContent: "center",
		marginBottom: 10,
	},
	buttonUpdate: {
		width: 130,
		backgroundColor: "lightgreen",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
        justifyContent: "center",
        marginTop: 10,
        marginBottom: 10,
	},
	buttonDelete: {
		width: 130,
		backgroundColor: "#FFCCCB",
		borderRadius: 10,
		height: 35,
		alignSelf: "center",
        justifyContent: "center",
        marginTop: 20,
        marginBottom: 10,
	},
	button: {
		alignSelf: "flex-end",
		justifyContent:'center',
		position:'absolute',
		width:60,
		height:60,
		backgroundColor:'grey',
		borderRadius:50,
		right:10,
		bottom:40,
	},
	TextInput: {
		borderWidth: 1,
        padding : 10,
        borderColor: 'black',
        height: 40,
        borderRadius: 50,
        marginTop: 10,
        marginBottom: 10,
        color: 'black',
		},
})

export default styles