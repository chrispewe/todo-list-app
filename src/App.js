import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import ActiveTaskScreen from './screens/ActiveTaskScreen/ActiveTaskScreen';
import AllTaskScreen from './screens/AllTaskScreen/AllTaskScreen';
import CompletedTaskScreen from './screens/CompletedTaskScreen/CompletedTaskScreen';
import ProfileScreen from './screens/ProfileScreen/ProfileScreen';
import LandingPage from './screens/LandingPage/LandingPage';
import LoginScreen from './screens/LoginScreen/LoginScreen';
import RegisterScreen from './screens/RegisterScreen/RegisterScreen';
import Icon from 'react-native-vector-icons/Ionicons';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();


function MyTabs() {
  return (
    <Tab.Navigator 
      initialRouteName="ProfileScreen" 
      tabBarOptions={{
        activeTintColor: "blue",
      }}
    >
      <Tab.Screen 
        name="ActiveTaskScreen"   
        component={ActiveTaskScreen} 
        options={{
          tabBarLabel: 'Active Task',
          tabBarIcon: ({ color, size }) => (
            <Icon name="alert" color={color} size={size} />
          ),
        }}  
      />
       <Tab.Screen 
        name="AllTaskScreen"   
        component={AllTaskScreen} 
        options={{
          tabBarLabel: 'All Task',
          tabBarIcon: ({ color, size }) => (
            <Icon name="list" color={color} size={size} />
          ),
        }}  
      />
      <Tab.Screen 
        name="CompletedTaskScreen"   
        component={CompletedTaskScreen} 
        options={{
          tabBarLabel: 'Completed Task',
          tabBarIcon: ({ color, size }) => (
            <Icon name="checkbox" color={color} size={size} />
          ),
        }}  
      />
       <Tab.Screen 
        name="ProfileScreen"   
        component={ProfileScreen} 
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color, size }) => (
            <Icon name="person-sharp" color={color} size={size} />
          ),
        }}  
      />
    </Tab.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="LandingPage" screenOptions={{headerShown: false}}>
        <Stack.Screen name="LandingPage" component={LandingPage} />
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
        <Stack.Screen name="Home" component={MyTabs} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
