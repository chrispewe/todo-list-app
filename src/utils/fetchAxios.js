import axios from 'axios';
import Config from 'react-native-config';

async function fetchRegister(val) {
    try {
        const response = await axios.post(`${Config.BASE_URL}/user/register`, val);
        console.log('res: == ', response.data)

    } catch (e) {
        console.error(e);
    }
}

async function fetchAxios(method, path, body, header) {
    try {
      const req = await axios({
        method,
        url: `${Config.BASE_URL}/${path}`,
        data: body,
        headers: header,
      });
  
      if (req.status !== 400) {
        return req;
      }
    } catch (e) {
      throw e;
    }
  
  }


export {fetchRegister, fetchAxios}